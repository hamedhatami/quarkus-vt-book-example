###  Run Postgres docker container

     docker run -d --rm --name my_reative_db -e POSTGRES_USER=user -e POSTGRES_PASSWORD=password -e POSTGRES_DB=my_db -p 5432:5432 postgres

### Add a book

     curl -i -X POST "http://localhost:8080/api/books" -H "Content-Type: application/json" -d '{"author":"book_author","category":"CLASSIC","publisher":"book_publisher","title":"book_title"}'

### Add a bookStore

     curl -i -X POST "http://localhost:8080/api/bookStores" -H "Content-Type: application/json" -d '{"name":"book_store_name","location":"Sweden"}'

### get all bookStores

     curl -i -X GET "http://localhost:8080/api/bookStores" -H "Content-Type: application/json"

### add a book to a bookStore

     curl -i -X POST "http://localhost:8080/api/bookStores/<bookStoreName>/book/<bookTitle>" -H "Content-Type: application/json"

### Get all books

     curl -i -X GET "http://localhost:8080/api/books" -H "Content-Type: application/json"

### update a Book

     curl -i -X PUT "http://localhost:8080/api/books/1" -H "Content-Type: application/json" -d '{"author":"book_author_1","category":"HISTORY","publisher":"book_publisher_1","title":"book_title_1"}'

### delete a Book

     curl -i -X DELETE "http://localhost:8080/api/books/2" -H "Content-Type: application/json"
