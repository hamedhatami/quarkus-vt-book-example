package org.example.book.util;

import jakarta.json.bind.Jsonb;
import jakarta.json.bind.JsonbBuilder;
import jakarta.json.bind.JsonbConfig;
import jakarta.json.bind.annotation.JsonbDateFormat;
import lombok.experimental.UtilityClass;

import java.nio.charset.StandardCharsets;
import java.util.Locale;

@UtilityClass
public class Constants {
    public static final Jsonb JSON_B = JsonbBuilder.create(new JsonbConfig()
            .withDateFormat(JsonbDateFormat.TIME_IN_MILLIS, Locale.getDefault())
            .withNullValues(false)
            .withEncoding(StandardCharsets.UTF_8.name()));
}
