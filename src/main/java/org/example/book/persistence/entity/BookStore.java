package org.example.book.persistence.entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import java.util.List;
import java.util.UUID;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class BookStore {
    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(
            name = "UUID",
            strategy = "org.hibernate.id.UUIDGenerator"
    )
    private UUID id;
    private String name;
    private String location;
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "bookStore_book",
            joinColumns = {@JoinColumn(name = "bookStoreId",
                    referencedColumnName = "id")},
            inverseJoinColumns = {@JoinColumn(name = "bookId",
                    referencedColumnName = "id")})
    private List<Book> books;

}
